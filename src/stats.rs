use std::sync::atomic::Ordering;

use crate::map::bucket::BUCKET_COUNTERS;

#[derive(Clone, Debug)]
pub struct GlobalStats {
    pub bucket_array_creation_count: u64,
    pub bucket_array_allocation_bytes: u64,
    pub bucket_array_drop_count: u64,
    pub bucket_array_release_bytes: u64,
    pub bucket_creation_count: u64,
    pub bucket_drop_count: u64,
}

impl GlobalStats {
    pub fn current() -> Self {
        let c = &BUCKET_COUNTERS;
        Self {
            bucket_array_creation_count: c.bucket_array_creation_count.load(Ordering::Acquire),
            bucket_array_allocation_bytes: c.bucket_array_allocation_bytes.load(Ordering::Acquire),
            bucket_array_drop_count: c.bucket_array_drop_count.load(Ordering::Acquire),
            bucket_array_release_bytes: c.bucket_array_release_bytes.load(Ordering::Acquire),
            bucket_creation_count: c.bucket_creation_count.load(Ordering::Acquire),
            bucket_drop_count: c.bucket_drop_count.load(Ordering::Acquire),
        }
    }
}
